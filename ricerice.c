#include <stdio.h>
#include <string.h>

#define LNLEN 180
#define LNNUM 299

int verbose = 1; // set verbosity on and off
int fuse = 0; // count file use

struct colorline {
	unsigned int linenum; // the line number
	unsigned int cola; // start of color
	unsigned short int collen; // length of color 6 or 3
}; // a line which has a color in it

typedef struct {
	FILE *f;
	char *fpath;
	struct colorline cl[LNNUM];
} fmap;

int isotherch(char c) {
	switch (c) {
	case '\t': case ' ': case '\'':
	case '\"': case '=': case ':':
		return 1; break;
	default:
		return 0; break;
	}
}

int iscolch(char c) {
	switch (c) {
	case '1': case '2': case '3': case '4': 
	case '5': case '6': case '7': case '8': 
	case '9': case '0':
	case 'a': case 'b': case 'c': case 'd':
	case 'e': case 'f': 
	case 'A': case 'B': case 'C': case 'D':
	case 'E': case 'F': 
		return 1;
	default:
		return 0;
	}
}

void rmfmap(fmap m) {
	fclose(m.f);
}

fmap mkfmap(char *filepath, char *mode) {
	fmap m; // make a fmap

	m.fpath = filepath; // give file path to fmap
	m.f = fopen(m.fpath, mode); // give file pointer to fmap

	rewind(m.f); // rewind file
	char s[LNLEN]; // string to store line

	for (int lnum=1, index=0, valid=0; (fgets(s, LNLEN, m.f)) != NULL; lnum++) {
		// inspect the line 
		for (int i=0, j=0, coli=1, athash=0; s[i] != '\n'; i++, j++) {
			// if hash is found
			if (athash) {
				if (iscolch(s[i])) {
					m.cl[index].collen = (coli == 6)? 6: 3; // detect color len
					if (coli>2) valid=1; // detect if valid color
					coli++;
				} else athash = 0;
			// find pound
			} else if (s[i] == '#') {
				coli = 1;
				athash = 1;
				m.cl[index].cola = j+1;
			}

			// discount spaces, tabs, and others
			if (isotherch(s[i])) j--; 
		}

		// if line is valid then add it
		if (valid) {
			valid = 0;
			m.cl[index].linenum = lnum;
			index++;
		}
	}

	if (verbose) printf("\n%d MAPPED the file\n", ++fuse);
	return m; // return the fmap
}

void writemap(fmap in, fmap out) {
	// rewind both files
	rewind(in.f);
	rewind(out.f);

	char sin[LNLEN]; // string of input file line
	char sout[LNLEN]; // string of output file line
	int linematches = 0; // does it match
	int edits = 0; // how many edits done - to be returned

	// inspect each line in first file
	for (int lnin = 1, index1 = 0; (fgets(sin, LNLEN, in.f)) != NULL; lnin++) {
		if (lnin != in.cl[index1].linenum) continue; // skip until designated lines are reached

		// inspect each line in second file against first file's lines
		for (int lnout = 1, index2 = 0; 1; lnout++) {

			long pos = ftell(out.f); // get position at start of line
			if ((fgets(sout, LNLEN, out.f)) == NULL) break; // for condition
			long endpos = ftell(out.f); // get position at start of next line

			if (lnout != out.cl[index2].linenum) continue; // skip until designated lines are reached
			else linematches = 0;

			// inspect chars on both lines
			int i = 0, j = 0, k = 0, l = 0;
			while (!sin[i] == '\0' && !sout[j] == '\0') {
				if (k == in.cl[index1].cola || l == out.cl[index2].cola) break; // if reached color then stop

				// if there is a proper char on both
				if (!isotherch(sin[i]) && !isotherch(sout[j])) {
					if (sin[i] == sout[j]) linematches = 1; // if they match say so
					else { // otherwise say no and break out of line comparison
						linematches = 0;
						break;
					}

					k++; l++; // count proper chars
					i++; j++; // add to both to go to next char
				// if there is proper char on one then increase other to catch up
				} else if (!isotherch(sin[i])) j++;
				else if (!isotherch(sout[j])) i++;
				// if no proper chars then just add as with both
				else { i++; j++; }
			}

			// after comparison if two strings matched
			if (linematches) {
				fseek(out.f, pos+j, SEEK_SET); // go to color chars in line, in output file

				// insert color codes from input file accordingly into position, in output file

				int samecolor = 0;
				// if colors in both files are 3 in length
				if (in.cl[index1].collen == 3 && out.cl[index2].collen == 3) {
					// test if same color
					for (int x = 0; x < 3; x++) {
						if (sin[i+x] == sout[j+x]) samecolor = 1;
						else { samecolor = 0; break; }
					}
					// if both colors aren't the same write color from input to output
					if (!samecolor) { // if both colors are not the same
						for (int x = 0; x < 3; x++) fputc(sin[i+x], out.f);
						edits++;
						if (verbose) printf("\t -- %s \t -> %s \n", sin, sout);
					}

				// if colors in both files are 6 in length
				} else if (in.cl[index1].collen == 6 && out.cl[index2].collen == 6) {
					// test if same color
					for (int x = 0; x < 6; x++) {
						if (sin[i+x] == sout[j+x]) samecolor = 1;
						else { samecolor = 0; break; }
					}
					// if colors aren't the same write input to output
					if (!samecolor) {
						for (int x = 0; x < 6; x++) fputc(sin[i+x], out.f);
						edits++;
						if (verbose) printf("\t -- %s \t -> %s \n", sin, sout);
					}

				// if colors in files have unmatching length 
				} else if (in.cl[index1].collen == 3 && out.cl[index2].collen == 6) {
					// test if same color
					for (int x = 0; x < 3; x++) {
						if (sin[i+x] == sout[j+(x*2)]) samecolor = 1;
						else { samecolor = 0; break; }
					}
					// distribute three chars on the six
					if (!samecolor) {
						for (int x = 0; x < 3; x++) {
							fputc(sin[i+x], out.f);
							fputc('0', out.f);
						}
						edits++;
						if (verbose) printf("\t -- %s \t -> %s \n", sin, sout);
					}

				// if colors in files have unmatching length 
				} else if (in.cl[index1].collen == 6 && out.cl[index2].collen == 3) {
					// test if same color
					for (int x = 0; x < 3; x++) {
						if (sin[i+(x*2)] == sout[j+x]) samecolor = 1;
						else { samecolor = 0; break; }
					}
					// cut six chars into the three
					if (!samecolor) {
						for (int x = 0; x < 6; x++) {
							if (x == 0 || x == 2 || x == 4) fputc(sin[i+x], out.f);
						}
						edits++;
						if (verbose) printf("\t == %s \t -> %s \n", sin, sout);
					}
				}

				// END insert color codes from input file accordingly into position, in output file

				fseek(out.f, endpos, SEEK_SET); // go back to end of line to repeat
			}

			if (lnout == out.cl[index2].linenum) index2++; // if on designated line add to index
		}

		rewind(out.f);

		if (lnin == in.cl[index1].linenum) index1++; // if on designated line add to index
	}

	if (edits) printf("%d OVERWRITTEN %d colors here\n", fuse, edits);
	else printf("%d SKIPPING because same colors\n", fuse);
}

void writetopack(fmap in, fmap pack) {
	rewind(in.f); // rewind input file
	char s[LNLEN]; // string store

	int counter = 0; // count lines written

	// inspect each line in first file
	for (int linenum = 1, index = 0; (fgets(s, LNLEN, in.f)) != NULL; linenum++) {
		// only on designated lines
		if (linenum == in.cl[index].linenum) {
			counter++; // add to counter as line will be written
			// print to file until end of line
			for (int i = 0, found = 0; s[i] != '\0'; i++) {
				// if pound and it is not found
				if (s[i] == '#' && !found) fprintf(pack.f, "%s", "   ");
				// insert only proper chars
				if (!isotherch(s[i])) fputc(s[i], pack.f);
			}
			index++; // get next designated line
		}
	}

	if (counter) printf("%d ADDING %d lines to pack\n", fuse, counter);
	else printf("%d SKIPPING because no colors found\n", fuse);
}

int main(int argc, char *argv[]) {
	if (argc < 4) {
		printf("ricerice --pack packfile file1 file2 file3\n");
		printf("ricerice --conf master slave1 slave2\n");
		return 2;
	}

	if ((strcmp(*(argv+1), "--pack")) == 0) {
		fmap pack = mkfmap(*(argv+2), "w");
		for (int i=0; i < argc-3; i++) {
			fmap conf = mkfmap(*(argv+i+3), "r");
			writetopack(conf, pack);
			rmfmap(conf);
		}
		rmfmap(pack);
	} else if ((strcmp(*(argv+1), "--conf")) == 0) {
		fmap master = mkfmap(*(argv+2), "r");
		for (int i=0; i < argc-3; i++) {
			fmap slave = mkfmap(*(argv+i+3), "r+");
			writemap(master, slave);
			rmfmap(slave);
		}
		rmfmap(master);
	}
}
